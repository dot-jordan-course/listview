package com.daly.listviewclass

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.widget.BaseAdapter
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)


        var profiles = ArrayList<Profile>()

        profiles.add(
            Profile(
                "Laura",
                "J. Tran",
                "1887 Larry Street",
                "https://www.fakenamegenerator.com/images/sil-female.png"
            )
        )
        profiles.add(
            Profile(
                "Yolanda",
                "J. Koester",
                "1887 Larry Street",
                "https://www.fakenamegenerator.com/images/sil-female.png"
            )
        )
        profiles.add(
            Profile(
                "Jess",
                "P. Sledge",
                "1887 Larry Street",
                "https://www.fakenamegenerator.com/images/sil-male.png"
            )
        )
        profiles.add(
            Profile(
                "Barbara",
                "E. Howard",
                "1887 Larry Street",
                "https://www.fakenamegenerator.com/images/sil-female.png"
            )
        )
        profiles.add(
            Profile(
                "Robert",
                "M. Sample",
                "1887 Larry Street",
                "https://www.fakenamegenerator.com/images/sil-male.png"
            )
        )
        profiles.add(
            Profile(
                "Laura",
                "J. Tran",
                "1887 Larry Street",
                "https://www.fakenamegenerator.com/images/sil-female.png"
            )
        )
        profiles.add(
            Profile(
                "Yolanda",
                "J. Koester",
                "1887 Larry Street",
                "https://www.fakenamegenerator.com/images/sil-female.png"
            )
        )
        profiles.add(
            Profile(
                "Jess",
                "P. Sledge",
                "1887 Larry Street",
                "https://www.fakenamegenerator.com/images/sil-male.png"
            )
        )
        profiles.add(
            Profile(
                "Barbara",
                "E. Howard",
                "1887 Larry Street",
                "https://www.fakenamegenerator.com/images/sil-female.png"
            )
        )
        profiles.add(
            Profile(
                "Robert",
                "M. Sample",
                "1887 Larry Street",
                "https://www.fakenamegenerator.com/images/sil-male.png"
            )
        )
        profiles.add(
            Profile(
                "Laura",
                "J. Tran",
                "1887 Larry Street",
                "https://www.fakenamegenerator.com/images/sil-female.png"
            )
        )
        profiles.add(
            Profile(
                "Yolanda",
                "J. Koester",
                "1887 Larry Street",
                "https://www.fakenamegenerator.com/images/sil-female.png"
            )
        )
        profiles.add(
            Profile(
                "Jess",
                "P. Sledge",
                "1887 Larry Street",
                "https://www.fakenamegenerator.com/images/sil-male.png"
            )
        )
        profiles.add(
            Profile(
                "Barbara",
                "E. Howard",
                "1887 Larry Street",
                "https://www.fakenamegenerator.com/images/sil-female.png"
            )
        )
        profiles.add(
            Profile(
                "Robert",
                "M. Sample",
                "1887 Larry Street",
                "https://www.fakenamegenerator.com/images/sil-male.png"
            )
        )
        profiles.add(
            Profile(
                "Barbara",
                "E. Howard",
                "1887 Larry Street",
                "https://www.fakenamegenerator.com/images/sil-female.png"
            )
        )
        profiles.add(
            Profile(
                "Robert",
                "M. Sample",
                "1887 Larry Street",
                "https://www.fakenamegenerator.com/images/sil-male.png"
            )
        )
        profiles.add(
            Profile(
                "Barbara",
                "E. Howard",
                "1887 Larry Street",
                "https://www.fakenamegenerator.com/images/sil-female.png"
            )
        )
        profiles.add(
            Profile(
                "Robert",
                "M. Sample",
                "1887 Larry Street",
                "https://www.fakenamegenerator.com/images/sil-male.png"
            )
        )
        profiles.add(
            Profile(
                "Barbara",
                "E. Howard",
                "1887 Larry Street",
                "https://www.fakenamegenerator.com/images/sil-female.png"
            )
        )
        profiles.add(
            Profile(
                "Robert",
                "M. Sample",
                "1887 Larry Street",
                "https://www.fakenamegenerator.com/images/sil-male.png"
            )
        )



        mListView.adapter = ProfileAdapter(applicationContext, profiles)

    }
}
