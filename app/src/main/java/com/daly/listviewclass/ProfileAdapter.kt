package com.daly.listviewclass

import android.content.Context
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.BaseAdapter
import android.widget.ImageView
import android.widget.TextView
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.profile_templete.view.*
import org.w3c.dom.Text

class ProfileAdapter(var context: Context, var profiles: ArrayList<Profile>) : BaseAdapter() {


    override fun getItem(position: Int): Profile {
        return profiles.get(position)
    }

    override fun getItemId(position: Int): Long {
        return position.toLong()
    }

    override fun getCount(): Int {
        return profiles.size
    }

    override fun getView(position: Int, convertView: View?, parent: ViewGroup?): View {


        var mainView: View
        var viewHolder: ViewHolder
        if (convertView == null) {

            var layoutInflater = LayoutInflater.from(context);
            mainView = layoutInflater.inflate(R.layout.profile_templete, parent, false)
            viewHolder = ViewHolder(mainView.mTextViewFullName, mainView.mTextViewAddress, mainView.mImageView)
            Log.d("MYAPP", "We are here for " + position)

            mainView.tag = viewHolder
        } else {
            mainView = convertView
            viewHolder = convertView.tag as ViewHolder
        }


        var profile: Profile = getItem(position)
        viewHolder.mTextViewFullName.text = profile.firstName + " " + profile.lastName
        viewHolder.mTextViewAddress.text = profile.address

        Picasso.get().load(profile.profilePicture).into(viewHolder.mImageView);

        return mainView
    }

    private class ViewHolder(var mTextViewFullName: TextView, var mTextViewAddress: TextView, var mImageView: ImageView)
}